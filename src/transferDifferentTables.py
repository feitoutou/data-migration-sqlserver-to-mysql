from connector import *

srcTableName = 'srcTableName'
destTableName = 'destTableName'

srcTableColumns = ['c1', 'c2', 'c3']
destTableColumns = ['c11', 'c12', 'c13', 'c14']

def mappingC3ToC1314(c3Value):
    if c3Value == 1:
        return [1,2]
    elif c3Value == 2:
        return [3,4]
    else:
        return [5,6] if c3Value > 3 else []
    
insertStatement = 'insert into %s values (%s)' % (destTableName, ('%s,'*len(destTableColumns))[:-1])

destCursor.execute('truncate %s' % destTableName)
destDb.commit()
rows = 0

srcCursor.execute('select * from %s' % srcTableName)
srcData = srcCursor.fetchall()

for data in srcData:
    values = [None] * len(destTableColumns)
    values[0] = data[0]
    values[1] = data[1]
    mappedValues = mappingC3ToC1314(data[2])
    if len(mappedValues) == 2:
        values[2] = mappedValues[0]
        values[3] = mappedValues[1]
    print(values)
    destCursor.execute(insertStatement, values)
    rows += 1
destDb.commit()
print(' Rows inserted: {} ({})'.format(destTableName, rows))