from connector import *

def getTotalColumns(tableName):
    destCursor.execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + Config.destSchema + "' AND TABLE_NAME = '" + tableName + "'")
    return len(destCursor.fetchall())

def getRowAmount(tableName):
    srcCursor.execute('select count(1) from ' + tableName)
    return srcCursor.fetchone()[0]

#Direct transfer data without any change
tables = [{'name': 'table1', 'orderBy': 'id'},
          {'name': 'table2', 'orderBy': 'id'}]

for table in tables:
    #truncate first
    destCursor.execute('truncate ' + table)
    destDb.commit()
    
    queryStatement = 'select * from ' + table['name'] + ' order by ' + table['orderBy']
    rows = getRowAmount(table['name'])
    columns = getTotalColumns(table['name'])
    params = ("%s," * columns)[:-1]
    insertStatement = 'insert into ' + table['name'] + ' values (%s)' % params
    print(' total records: ' + str(rows))
    try:
        transferred = 0
        for pageNumber in range(int(rows / Config.pageSize) + 1):
            limitStatement = ' offset ' + str(Config.pageSize * pageNumber) + ' rows fetch next ' + str(Config.pageSize) + ' rows only'
            srcCursor.execute(queryStatement + limitStatement)
            results = srcCursor.fetchall()
            for result in results:
                destCursor.execute(insertStatement, [x for x in result])
                transferred += 1
        destDb.commit()
        print(str(transferred) + ' records transferred ... ')
    except Exception as e:
        print(' -- error happened while transferring data for table ' + table['name'] + ' --')
        print(e)
        destDb.rollback()
        closeConnections()
        sys.exit()
    print(table['name'] + ' transfer is successfully completed ~~~~ ')
