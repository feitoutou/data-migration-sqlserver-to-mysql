import traceback, threading, time
from connector import *

#If rows in a table is over 100k, it's a good idea to use multi threading and batch insert
#But pay attention to relationship and dependency of table data
#Only suitable for row data is not relating to or depending on other row data

#The idea of multi threading is to split the src data into several sections and use a thread for each section, also you can use pagination inside each section
#For example, total rows = 2402647, threads = 10, then sections are:
# [[0, 240264], [240264, 480528], [480528, 720792], [720792, 961056], [961056, 1201320], [1201320, 1441584], [1441584, 1681848], [1681848, 1922112], [1922112, 2162376], [2162376, 2402647]]
threads = []
maxThreads = 10
batch_insert_rows = 100
large_table_columns = ['c1', 'c2', 'c3', 'c4', 'c5']
                    #    0    1      2    3      4
                    
def transferSection(section, insertStatement):
    print('Thread started for section: {}'.format(section))
    pages = int((section[1] - section[0]) / Config.pageSize) + 1
    
    #need to create new connection for each thread
    srcDbThread = getSqlServerConnection()
    srcCursorThread = srcDbThread.cursor()
    destDbThread = getDestMySqlConnection()
    destCursorThread = destDbThread.cursor(buffered=True)
    
    for pageNumber in range(pages):
        try:
            pagination = ' order by id offset ' + str(section[0] + Config.pageSize * pageNumber) + ' rows fetch next ' + ( str(Config.pageSize) if pageNumber < pages - 1 else str(section[1] - (section[0] + Config.pageSize * pageNumber)))+ ' rows only'
#             print(pagination)
#             continue
            srcCursorThread.execute("select * from large_table " + pagination)
            sectionPageData = srcCursorThread.fetchall()
            valueArray = []
            for data in sectionPageData:
                values = [None] * len(large_table_columns)
                values[0] = data[0]
                values[1] = data[1]
                #write your own logic here
                
                valueArray.append(values)
                if len(valueArray) % batch_insert_rows == 0:
                    destCursorThread.executemany(insertStatement, valueArray)
                    destDbThread.commit()
                    valueArray = []
                    print(' Thread {} {} rows transferred '.format(threading.current_thread(), batch_insert_rows))
            if len(valueArray) > 0:
                destCursorThread.executemany(insertStatement, valueArray)
                destDbThread.commit()
                print(' {} rows transferred in Thread {}'.format(len(valueArray), threading.current_thread()))
                
        except Exception as e:
            print(e)
            traceback.print_stack()
            destDbThread.rollback()
            closeConnections()
            srcDbThread.close()
            destDbThread.close()
            sys.exit()        

class transferSectionThread(threading.Thread):
    def __init__(self, section, insertStatement):
        threading.Thread.__init__(self)
        self.section = section
        self.insertStatement = insertStatement
    def run(self):
        transferSection(self.section, self.insertStatement)

start = time.time()

try:
    destCursor.execute('truncate large_table')
    destDb.commit()
    print(' large_table cleared!')
    srcCursor.execute("select count(1) as rows from large_table")
    rows = srcCursor.fetchone()[0]
    print('Ready to transfer large_table, total rows = {}'.format(rows))
    insertStatement = 'insert into large_table values (%s)' % ('%s,'*len(large_table_columns))[:-1]
    factor = int(rows / maxThreads)
    section = []
    for index in range(maxThreads):
        section.append([factor * index, factor * (index + 1) if index < maxThreads - 1 else rows])
    print(section)
    
    for index in range(maxThreads):
        thread = transferSectionThread(section[index], insertStatement)
        thread.start()
        threads.append(thread)
    for t in threads:
        t.join()
    print(' large_table transferred successfully ')
except Exception as e:
    print(e)
    traceback.print_stack()
    destDb.rollback()
    closeConnections()
    sys.exit()
    
    
costTime = time.time() - start
print(' total time cost {}m {}s'.format(int(costTime/60), int(costTime%60)))
