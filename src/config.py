class Config():
    pageSize = 10000
    
    srcHost = '192.168.16.25,1433'
    srcUser = 'sql_server_user'
    srcPassword = 'sql_server_password'
    srcSchema = 'srcDbName'
    
    destHost = '192.168.16.52'
    destUser = 'mysql_user'
    destPassword = 'mysql_password'
    destSchema = 'destDbName'
    
