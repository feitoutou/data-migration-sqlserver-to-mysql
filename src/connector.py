import pyodbc, sys
import mysql.connector
from config import Config

def getSqlServerConnection():
    try:
        connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER='+ Config.srcHost +';DATABASE='+Config.srcSchema+';UID='+Config.srcUser+';PWD='+ Config.srcPassword
        cnxn = pyodbc.connect(connection_string)
        print('source sql server connection established')
        return cnxn
    except:
        print(' !Error source sql server can not be connected !' )
        sys.exit()
        
def getDestMySqlConnection(database=Config.destSchema, host=Config.destHost, user=Config.destUser, passwd=Config.destPassword):
    return mysql.connector.connect(
      host=host,
      user=user,
      passwd=passwd,
      database=database
      )

def closeConnections():
    destCursor.close()
    destDb.close()
    srcCursor.close()
    srcDb.close()
        
try:
    destDb = getDestMySqlConnection()
    destCursor = destDb.cursor()
    srcDb = getSqlServerConnection()
    srcCursor = srcDb.cursor()
    print('cursors are ready')
except Exception as e:
    print(' ---- unable to get server connections ---- ')
    print(e)
    closeConnections()
    sys.exit()