# data-migration-sqlserver-to-mysql

database migration script using Python 3.7
from sql server to mysql

library used: pyodbc, mysql.connector

you can find how to transfer data for different table structures
and
using multi threading / pagination / batch insert for large table data transfer

the script should be customized for your practical use - see config.py and table/column names in sample scripts

your comments and advice are welcomed
email to: alwingao@gmail.com

